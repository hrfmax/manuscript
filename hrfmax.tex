\documentclass[man]{apa6}

% packages
\usepackage{authblk}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage[natbibapa]{apacite}

\title{Why unrotated is still best -- Procrustes rotation ...}
\shorttitle{Why unrotated is still the best}
\leftheader{B\"{o}lts, Helmsauer, Woodward}

\threeauthors{Jan B\"{o}lts}{Konstantin Helmsauer}{Todd S. Woodward}
\threeaffiliations{Institute of Cognitive Science, Osnabr\"{u}ck}{Institute of Computer Science, T\"{u}bingen}{Department of Psychiatry, Vancouver}


\abstract{Read the whole paper, man.}

\begin{document}
\maketitle
\section*{Introduction}
Principal Component Analysis (PCA) allows to explore latent structure in multidimensional observations by projecting data into a component space of lower dimension. It has been proven to be an extremely useful tool to get insight in hidden properties of a complex dataset in a wide variety of sciences. PCA and derived methods as Independent Component Analysis (ICA) or Constrained PCA (CPCA) have also gained popularity in fMRI research as means to identify functionally idenpendent brain networks that jointly account for activity patterns gained from event-related fMRI (REFS?). However, sensible component rotation is a topic that still has to be considered for any of these analyses. Component analysis produces a component space that accounts for a maximum of variation in the data. Rotating the components defining this space will alter the variance accounted for by each component; however, the overall variance accounted for by all components will not change. Consequently, each rotation provides an equally valid framework of interpretation. It is thus of particular importance to choose the rotation that yields the scientifically most meaningful results. Rotation can be either orthogonal or oblique, preserving the orthogonality of unrotated components in the first case and relaxing this assumption in the second.

The most common objective of component rotation is to obtain simple structure solutions \citep{thurstone1947}. As variables often cluster together in their correlations with the different components emerging from PCA, it would be sensible to find a rotated solution such that components can be associated with specific clusters. In other words, simple structure is present and components are most easily interpretable if each component is strongly correlated with some variables but very weakly correlated with the remaining majority. With regard to functional brain networks, simple structure rotation causes voxel activities to be highly correlated with only one network and have near-zero correlations with the remaining networks.

Many methods have been developed to compute a simple structure rotation analytically. One of the most popular orthogonal rotation methods, Varimax, was proposed by \citet{kaiser1958}. Other methods such as Promax \citep{hendrickson1964} allow rotation to be oblique. For a review and comparison of various methods, see \citet{richman1986} or \citet{dien2010}. Nevertheless, it has to be questioned whether assuming simple structure to be immament in brain networks is valid at all.

In the following, a novel rotation method is developed for an extension of PCA which is especially useful within fMRI research: Constrained principal component analysis (CPCA) combines multiple multivariate regression and principal component analysis to explore structure in the variance of criterion variables that can be explained by variability in the predictor variables \citep{hunter2002, takane2001, takane1991}. It has been employed in a wide range of fields including animal studies \citep{hyman2013}, personality research \citep{lavigne2013}, fMRI \citep{woodward2006, metzak2011, rapin2012} and semi-conductor research \citep{cho2007}.
%
In a first step, the matrix of standardized criterion data $(Z)$ is divided into predicted scores $(GC)$ and residual scores $(E)$ via a multivariate multiple regression of $Z$ on a matrix of predictor variables, dummy-coded in $G$.
\begin{equation}
Z = GC + E
\label{eqn:e1}
\end{equation}
In a second step, principal component analysis (PCA) is applied to the matrix of predicted scores from the first step, $GC$, in order to explore its underlying component structure. By applying PCA to $GC$ instead of the original data matrix, $Z$, variance unrelated to the variables of interest is excluded from the resulting components. PCA is equivalent to Singular Value Decomposition (SVD), such that 
\begin{equation}
GC = UDV'
\label{eqn:e2}
\end{equation}
where $UU'=I$, $VV'=I$ and $D$ is the diagonal matrix of singular values. Further analysis can then be limited to a subset of singular values in $D$ and columns in $U$ and $V$, thereby extracting a certain number of components. The component loadings $VD/\sqrt{N}$ (where $N$ is the number of observations) can be interpreted as the correlation coefficients between component scores $U$ and original criterion variables $Z$. The component scores in $U$ represent the importance of each component to each of the variables in $GC$. In order to relate the predictor variables coded in $G$ to the components scores, a matrix of predictor weights $P$ is introduced, such that $U=GP$. $P$ consists of weights that, when applied to the predictor variables in $G$, form the component scores in $U$. Once components have been identified from the predicted scores in $GC$, component rotation can be applied to ease component interpretability.

Metzak et al. (2011) provide an example of CPCA applied to event-related blood-oxygen level dependent (BOLD) signal time-series data in a verbal working memory task. As in any fMRI-based CPCA (e.g. Woodward et al., 2006), $Z$ is an $n \times m$ matrix of $n$ scans and $m$ voxels and contains normalized, smoothed and subject mean-centered BOLD response values for each voxel in each scan. $G$ is an $n \times v$ matrix where $n =$  \# scans and $v =$ \# subjects $\times$ \# experimental conditions $\times$ \# time bins. $G$ is dummy-coded such that the scans for which the BOLD signal amplitude is to be estimated contain a value of 1. After regressing out variance that is not due to subjects, conditions and stimulus timing, principal components are extracted (equation \ref{eqn:e2}). These underlying patterns of activation can be interpreted as functional brain networks whose activity correlates with the activity of single voxels according to entries in $V$ and whose activity varies over scans as shown in $U$. $P$ contains predictor weights which reflect the estimated hemodynamic response of a functional network for each subject, condition and time bin. $P$ can thus be thought of as summarizing functional features of the explored networks. These predictor weights can be analyzed statistically to determine the effect of, for example, different conditions on the network activity identified by CPCA.

\citet{metzak2011} also summarize advantages of CPCA for event-related fMRI data analysis. Since it is an exploratory method that derives functional networks from the entire brain, specification of regions of interest, as in dynamic causal modeling or structural equation modeling, is not required. In addition, unlike unconstrained PCA or ICA, CPCA is a model-based method including information about stimulus timing. It therefore results in components that are based only on the model and excludes external sources of variance.

Component rotation in CPCA can be based on any of the matrices resulting from SVD, as long as the obtained rotation matrix in turn is applied to all other matrices. Simple structure rotation has been applied to the component loadings in $VD/\sqrt{N}$ in previous applications of CPCA on fMRI data \citep{woodward2006}. By promoting simple structure of component loadings, this approach leads to more clear-cut brain networks, as a certain voxel can be more clearly attributed to a certain network. Yet, for a reasonable interpretation of the results, we would have to assume underlying brain networks to be of simple structure. However, rotation methods can also be based on the predictor-weights matrix $P$. The resulting rotation matrix T could then optimize a given criterion on the rotated predictor weight matrix $P^{*}$ and be subsequently applied to $U$ and $VD$ to reconstruct a consistent component structure.

Research on event-related fMRI has led to a better understanding of the hemodynamic response function (HRF) and its dependency on cognitive events (references). This knowledge can be beneficially incorporated in fMRI-CPCA by constraining the estimated hemodynamic response of a network to realistic response behavior. 

We therefore propose a new rotation method, HRFMAX, that rotates principal components such that each time series of BOLD responses predicted in $P$ optimally fits one out of a pool of suggested HRF shapes. Instead of optimizing our rotation for component loadings in $VD\sqrt{N}$, we base this approach on the rotation of predictor-weights matrix $P$. A very similar approach has been used in a previous paper by \citet{metzak2011}. In the following, we use Monte-Carlo simulation to validate HRFMAX and show its superiority over simple structure based rotation methods. 
%
%
%
\section*{Methods}
In order to simulate the performance of HRFMAX and various other methods of components rotation, a theoretical model containing simulated fMRI data was constructed. The model was based on ideal artificial HRF shapes and complemented by original component loadings obtained from a CPCA on sample fMRI-dataset \citep{metzak2011}. In a Monte Carlo simulation, PCA was applied on the theoretical model for each simulation step before performing the component rotations. For the quantification of the performance of each of the component rotation methods, the resulting rotated predictor weights $P^*$ were compared to the underlying artificial HRF shapes that were used to generate the model. 

\subsection{The Theoretical Model}
The simulated fMRI time series used in the current simulation followed a block design. Ten subjects were included in the theoretical model. Each of them performed one run composed of 2 trials of either 1 or 2 conditions, resulting in \# of trials $\times$ \# of conditions scans per subject. Different theoretical models were built, including either 1 or 2 conditions and 2, 4, 6 or 8 underlying functional brain networks. For each underlying functional brain network we generated realistic HRF shapes based on random onset and duration parameters. Statistical Parametric Modeling software (Wellcome Institute of Cognitive Neurology, London, UK, http://www.fil.ion.ucl.ac.uk/spm) was used to generate single-trial time series based on the onset and duration of a hypothetical cognitive event (randomly chosen from 0-8 sec for onsets and 0-4 sec for duration). These were predicted by a canonical HRF and convolved with a delta function. Furthermore, within each model, half of our underlying networks were permitted to have qualitatively different HRF shapes for different conditions. The created shapes were arranged in the matrix of predictor weights, $P_{model}$. The theoretical component scores $U_{model}$ are then given by $U_{model}=GP_{model}$, where $G$ corresponds to the design matrix constructed in accordance with the chosen block design.

In addition, each of the different models included theoretical component loadings, given by $V_{model}$,  which were obtained from CPCA on original fMRI data \citep{metzak2011}. The final theoretical model $M$ was obtained by taking the matrix product of the theoretical component scores and loadings, $M=U_{model}V_{model}'$. In order to allow for comparison between different degrees of simple structure present in a model, both unrotated and Varimax rotated $V_{model}$ were used. Finally, $M$ was normalized column wise. In total, this resulted in 16 different combinations of model parameter (4 different numbers of underlying components $\times$ 2 different numbers of conditions $\times$ 2 different degrees of simple structure).

\subsection{The Simulation}
In order to compare different rotation methods, 5 Monte-Carlo simulations were conducted for each combination of model parameters, adding 0, 25, 50, 75 or 100 percent of normally-distributed noise, respectively. One Monte-Carlo simulation consisted of 200 iterations.
 
For each iteration, new random events, and thus a new theoretical model $M$ were created. After adding noise to $M$, SVD was applied, such that:
\begin{equation}
U_{unr}D_{unr}V_{unr} = M,
\label{eqn:e3}
\end{equation}                                    
where $U_{unr} =$ matrix of left singular vectors, $D_{unr} =$ diagonal matrix of singular values, $V_{unr} =$ matrix of right singular vectors and the subscript \textit{unr} stands for \textit{unrotated}. The matrix of predictor weights, $P_{unr}$, is obtained from $U_{unr}$ and the Moore Penrose pseudoinverse of $G$: $P_{unr}=G^{\dagger}U_{unr}$. For a given iteration, the component structure is rotated according to various rotation methods. The resulting rotated predictor weights $P^*$ and component loadings $V^*$ are compared to original $P_{model}$ and $V_{model}$ matrices, respectively. A more detailed description of the rotation methods we investigated goes here: 
%
%
\subsubsection{Varimax}
Varimax rotation finds an orthogonal rotation matrix such that the loadings of each of the principle components (columns in $V_{unrot}*D/sqrt(N)$) maximize the Varimax criterion \citep{kaiser1958}
\begin{equation}
V^{*}_j = \frac{n \sum_{i=1}^n (b_{ij}^2)^2 - \left( \sum_{i=1}^n b_{ij}^2 \right)^2 }{n^2}     j = 1, ... , r. 
\label{eqn:e4}
\end{equation}
Here $n$ is the number of variables, $r$ is the number of extracted principle components and $b_{ij}$ is the loading of the $i$th variable on the $j$th component. In other words, this criterion maximizes the variance of the squared loadings and attempts to obtain loadings that approach either $0$ or $\pm 1$. It thereby produces a rotated loading matrix that is as close as possible to a simple structure. Such a simple structure is present if the variables in the loading matrix have high loadings only on a single component and near-zero loadings on all other components.
 
We applied Varimax rotation on $V_{unr}$ and rotated the predictor weights using the obtained rotation matrix $T_{varimax}$: 
$V_{varimax}=V_{unr}T_{varimax}$; $P_{varimax}=P_{unr}T_{varimax}$. 
%
%
\subsubsection{Promax}
Promax rotation is an oblique simple structure rotation proposed by \citet{hendrickson1964}. The underlying rationale is that an orthogonal simple-structure solution is already quite close to an optimal oblique one. Therefore, Varimax is first used to produce an orthogonal simple structure solution $A^*$ from the unrotated component loadings $V$. This matrix $A^*$ is normalized by dividing each row by its Euclidean norm. Subsequently each column is divided by its largest absolute value. The entries in the resulting matrix are raised to the power of a parameter $m$, while preserving the original signs of the entries. In the simulation, we used $m = 4$, as recommended by \citet{hendrickson1964}. This last step results in a matrix that serves as a target matrix in an oblique procrustes rotation where $A^*$ is the matrix to be rotated. The resulting rotation matrix is applied to $A^*$ to obtain the Promax rotated component structure. Promax rotation has been previously applied for fMRI-CPCA \citep{woodward2006}.
%
%
\subsubsection*{HRFMAX}
HRFMAX rotation is an orthogonal component rotation method based on knowledge about the shape of the hemodynamic response function (HRF) contained in the predictor weights $P$.  As $P$ is a function of $U$, HRFMAX is -- in contrast to more common rotation methods -- not based on the scaled component loadings $V$ but rather on the scaled component scores $U$. A rotation matrix $T$ used to rotate $P$ can subsequently be applied to the component scores $U$ and the component loadings $VD$, which results in a rotated but still consistent component structure: $P^*=PT$, $U^*=UT$, $(VD)^*=VDT$. 

Knowledge about the experimental paradigm can be used to propose several cognitive events and corresponding HRF shapes are generated using SPM2. The HRF shapes are saved in a $P_{target}$ matrix that has the same dimensions as the predictor weights $P$.  Depending on the number of proposed events, several $P_{target}$ matrices are created, one for each combination of different HRF shapes in different experimental conditions and components. For each $P_{target}$, we use orthogonal procrustes rotation \citep{schonemann1966} to find an orthogonal rotation matrix $T$ for
\begin{equation}
					P_{target} = PT + E
\end{equation}
such that the sum of squares of the residual matrix is minimized: $argmax trace(E'E)$. For each $P_{target}$, we obtain a rotation matrix $T$ which rotates $P$ to become as similar to $P_{target}$ as possible. The absolute value of Pearson's correlation coefficient is computed between the columns of each $P_{target}$ and the respective rotated $P^*$. The $P_{target}$ producing the best fit is then chosen based on the product of the maximum values derived for each column of $P^*$. The resulting $T$ is used to rotate the whole component space, $P$, $U$ and $VD$.
 
This approach can lead to high computational cost as the number of components, proposed shapes and conditions increases. Computational cost can be reduced by restricting $P_{target}$ matrices to allow for only one HRF shape per component, regardless of the number of conditions. However, this comes at the expense of the flexibility of allowing multiple HRF shapes to emerge for different conditions within a single component. Nevertheless, network activity may still differ in scale for different conditions. If it is unlikely that a functional brain network is engaged in cognitive events whose HRF shapes do not only differ in scale, restricting to the same HRF shape within one component might be acceptable. Overall, HRFMAX provides a means to achieve the most meaningful component structures, as the extracted brain networks are restricted to show as realistic HRF shapes as possible. A similar precursor method has been already successfully applied for fMRI-CPCA \citep{metzak2011}.

Brain networks related to an experimental manipulation can never be completely uncorrelated. That is why orthogonality of components is an assumption that can be challenged. Therefore, enabling components to become correlated after oblique rotation might be preferable. In addition to orthogonal HRFMAX, we explored an oblique version using oblique procrustes rotation \citep{hurley1962}. Among other more subtle restrictions, the oblique rotation method requires $P_{target}$ to be of full rank. However, this requirement should not affect final results, as different components account for different sources of variance.
%
%
\subsection{Comparison of rotation methods}
For each method of rotation, the resulting rotated matrices of predictor weights $P^*$ were compared to the original matrix of predictor weights $P_{model}$, in order to quantify the similarity between each prediction and the theoretical model. As the order of components in matrices of the rotated component structure is arbitrary, pairs of model components and rotated components were matched. As a measure of similarity between $P_{model}$ and a rotated $P^*$, we chose the median of the absolute Spearman correlations between corresponding pairs of columns in both matrices. Spearman correlations were used as one cannot assume the HRF values to be normal distributed. The median of these values was calculated to get an appropriate measure of central tendency even if the distribution of Spearman correlations is skewed and component predictor weight pairs differ in their respective functional relation.

In addition, the question arises whether the component rotation maintain the loading structure in the model, e.g., the original or random component loadings $V_{model}$ used to built the model initially.  The similarity of the matrix of scaled theoretical component loadings $V_{model}$ and the rotated $V^*$ was quantified with the median of the column wise coefficient of congruency. This measure has been demonstrated as a suitable method for comparing loadings from different component structures \citep{tucker1951, lorenzoseva2006}. In our case, it is given by 
\begin{equation}
r_c = \frac{\sum_{i} V_{rotated}(i,c) V_{model}(i,c)}{ \left[ \sum_i V_{rotated}(i,c)^2 \sum_{i} V_{model}(i,c)^2 \right]^{\frac{1}{2}} }
\label{eqn:e5}
\end{equation}
where $V_{rotated}$ is rotated loading matrix, $V_{model}$ is original loading matrix, $c$ is the column and $i$ is the row. Both median of Spearman correlations and median of congruence coefficient were computed for each iteration. 

In order to explore significant differences between the outcomes of the applied rotations, two Wilcoxon signed rank tests were carried out for each pair of rotation methods in each simulation: The first tested for different central tendencies of the median absolute Spearman correlations between columns in $P_{model}$ and $P^*$. The second tested for different central tendencies of the median coefficient of congruency between $V_{model}$ and $V^*$. By using Wilcoxon signed rank test, assumptions about the distributions of the similarity measures were avoided and statistical dependency between samples was accounted for. 

\bibliographystyle{apacite}
\bibliography{hrfmax}

\end{document}